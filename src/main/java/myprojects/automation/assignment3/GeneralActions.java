package myprojects.automation.assignment3;

import myprojects.automation.assignment3.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    private By loginButtonName = By.name("submitLogin");
    private By employeeInfos = By.id("employee_infos");
    private By catalogueLink = By.id("subtab-AdminCatalog");
    private By categoriesLink = By.id("subtab-AdminCategories");
    private By addCategory = By.id("page-header-desc-category-new_category");
    private By nameOfTheCategory = By.className("copy2friendlyUrl");
    private By saveCategory = By.id("category_form_submit_btn");
    private By filterByName = By.xpath("//*[@id=\"table-category\"]/thead/tr[1]/th[2]/span/a[1]");
    private By categoryNameInTable = By.xpath("//*[@id=\"table-category\"]/tbody/tr[1]/td[3]");
    private By categoryCreatedAlert = By.cssSelector(".alert.alert-success");

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        try {
            driver.navigate().to(Properties.getBaseAdminUrl());
            waitForContentLoad(loginButtonName);
            WebElement loginInput = driver.findElement(By.id("email"));
            WebElement passInput = driver.findElement(By.id("passwd"));
            WebElement loginButton = driver.findElement(By.className("ladda-label"));
            loginInput.sendKeys(login);
            passInput.sendKeys(password);
            loginButton.click();
            waitForContentLoad(employeeInfos);
        } catch (Throwable e) {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Adds new category in Admin Panel.
     * @param categoryName
     */
    public void createCategory(String categoryName) {
        try {
            Actions actions = new Actions(driver);
            WebElement catalogueLink = driver.findElement(this.catalogueLink);
            actions.moveToElement(catalogueLink).perform();
            waitForContentLoad(this.catalogueLink);
            WebElement categoriesLink = driver.findElement(this.categoriesLink);
            actions.moveToElement(categoriesLink).click().perform();
            waitForContentLoad(this.categoriesLink);
            WebElement addCategory = driver.findElement(this.addCategory);
            addCategory.click();
            waitForContentLoad(this.saveCategory);
            WebElement nameOfTheCategory = driver.findElement(this.nameOfTheCategory);
            nameOfTheCategory.sendKeys(categoryName);
            WebElement saveCategory = driver.findElement(this.saveCategory);
            saveCategory.click();
            waitForContentLoad(this.categoryCreatedAlert);
            WebElement filterByName = driver.findElement(this.filterByName);
            filterByName.click();
            waitForContentLoad(this.categoryNameInTable);
        } catch (Throwable e) {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad(By element) {
        this.wait.until(ExpectedConditions.presenceOfElementLocated(element));
    }

}
