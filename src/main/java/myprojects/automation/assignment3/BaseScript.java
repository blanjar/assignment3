package myprojects.automation.assignment3;

import myprojects.automation.assignment3.utils.EventHandler;
import myprojects.automation.assignment3.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {

    /**
     *
     * @return New instance of {@link WebDriver} object. Driver type is based on passed parameters
     * to the automation project, returns {@link ChromeDriver} instance by default.
     */
    public static WebDriver getDriver() {
        String browser = Properties.getBrowser();
        WebDriver driver;
        switch (browser) {
            case "internet explorer":
                    System.setProperty(
                            "webdriver.ie.driver",
                            new File(BaseScript.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                    driver = new InternetExplorerDriver();
                    break;
            case "MicrosoftEdge":
                    System.setProperty(
                            "webdriver.ie.driver",
                            new File(BaseScript.class.getResource("/MicrosoftWebDriver.exe").getFile()).getPath());
                    driver = new InternetExplorerDriver();
                    break;
            case "firefox":
                    System.setProperty(
                            "webdriver.gecko.driver",
                            new File(BaseScript.class.getResource("/geckodriver.exe").getFile()).getPath());
                    System.out.println("Step");
                    driver = new FirefoxDriver();
                    break;
            default:
                    System.setProperty(
                            "webdriver.chrome.driver",
                            new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
                    driver = new ChromeDriver();
        }
        driver.manage().window().maximize();
        return driver;
    }

    /**
     * Creates {@link WebDriver} instance with timeout and browser window configurations.
     *
     * @return New instance of {@link EventFiringWebDriver} object. Driver type is based on passed parameters
     * to the automation project, returns {@link ChromeDriver} instance by default.
     */
    public static EventFiringWebDriver getConfiguredDriver() {
        try {
            WebDriver driver = getDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            EventFiringWebDriver wrappedDrvier = new EventFiringWebDriver(driver);
            wrappedDrvier.register(new EventHandler());
            return wrappedDrvier;
        } catch (Throwable e) {
            throw new UnsupportedOperationException("Method doesn't return configured WebDriver instance");
        }
    }
}
