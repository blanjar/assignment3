package myprojects.automation.assignment3.tests;

import myprojects.automation.assignment3.BaseScript;
import myprojects.automation.assignment3.GeneralActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateCategoryTest extends BaseScript {
    private static String login = "webinar.test@gmail.com";
    private static String password = "Xcg7299bnSmMuRLp9ITw";

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getConfiguredDriver();
        // ...
        GeneralActions actions = new GeneralActions(driver);

        // login
        actions.login(login, password);

        // create category
        String categoryName = "New Category " + System.currentTimeMillis();
        actions.createCategory(categoryName);

        // check that new category appears in Categories table
        By categoryNameInTable = By.xpath("//*[@id=\"table-category\"]/tbody/tr[1]/td[3]");
        WebElement filteredCategoryNameEl = driver.findElement(categoryNameInTable);
        String filteredCategoryName = filteredCategoryNameEl.getText();
        if(!categoryName.equals(filteredCategoryName)) {
            System.out.println("Category Names does not match: " + categoryName + " - " + filteredCategoryName);
        } else {
            System.out.println("New category: " + categoryName + " found in Categories table");
        }

        // finish  script
        driver.quit();
    }
}
